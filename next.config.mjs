/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'sgp1.digitaloceanspaces.com',
        pathname: '**',
      },
    ],
  },
};

export default nextConfig;